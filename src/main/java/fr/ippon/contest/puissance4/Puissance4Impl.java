package fr.ippon.contest.puissance4;

/**
 *
 * @author Ludovic TOISON, M2 MIAGE SITN, Université Paris-Dauphine.
 */
public final class Puissance4Impl implements Puissance4 {

    private EtatJeu etatJeu;

    private char joueurCourant;

    private char[][] grilleJeu;

    private final int NOMBRE_LIGNES = 6;

    private final int NOMBRE_COLONNES = 7;

    public Puissance4Impl() {
        this.nouveauJeu();
    }

    @Override
    public void nouveauJeu() {
        // On positionne l'état du jeu à en cours
        this.etatJeu = EtatJeu.EN_COURS;

        // On génère un nombre aléatoire : 1=R, 0=J
        this.joueurCourant = (Math.round(Math.random() * 1) == 1) ? 'R' : 'J';

        // On remplit la grille selon les dimensions
        this.initialiserGrilleJeu();
    }

    private void initialiserGrilleJeu() {
        this.grilleJeu = new char[NOMBRE_LIGNES][NOMBRE_COLONNES];

        for (int i = 0; i < NOMBRE_LIGNES; i++) {
            for (int j = 0; j < NOMBRE_COLONNES; j++) {
                this.grilleJeu[i][j] = '-';
            }
        }
    }

    @Override
    public void chargerJeu(char[][] grille, char tour) {
        // On teste les dimensions de la grille
        if (grille.length != NOMBRE_LIGNES) {
            throw new IllegalArgumentException("Les dimensions de la grille sont invalides (nombre de ligne)" + grille.length + " : " + NOMBRE_LIGNES);
        }
        for (int i = 0; i < NOMBRE_LIGNES; i++) {
            if (grille[i].length != NOMBRE_COLONNES) {
                throw new IllegalArgumentException("Les dimensions de la grille sont invalides (nombre de colonnes)" + grille[i].length + " : " + NOMBRE_COLONNES);
            }
        }
        // On teste le joueur
        if (tour != 'J' && tour != 'R') {
            throw new IllegalArgumentException("Le joueur saisi est invalide");
        }
        this.etatJeu = EtatJeu.EN_COURS;
        this.grilleJeu = grille;
        this.joueurCourant = tour;
        // On teste d'abord la configuration du joueur qui vient de jouer (donc le prochain)
        testerJeu(this.getProchain());
        // Puis on teste la configuration du joueur dont c'est le tour
        testerJeu(this.joueurCourant);
    }

    @Override
    public EtatJeu getEtatJeu() {
        return this.etatJeu;
    }

    @Override
    public char getTour() {
        return this.joueurCourant;
    }

    /**
     * @return le joueur suivant : opposé du joueur courant.
     */
    public char getProchain() {
        return (this.joueurCourant == 'R') ? 'J' : 'R';
    }

    @Override
    public char getOccupant(int ligne, int colonne) {
        if ((ligne >= 0 && ligne < NOMBRE_LIGNES) && (colonne >= 0 && colonne < NOMBRE_COLONNES)) {
            return this.grilleJeu[ligne][colonne];
        } else {
            throw new IllegalArgumentException("Les coordonnées saisies sont invalides");
        }
    }

    @Override
    public void jouer(int colonne) {
        // Le jeu est dans l'état en cours
        if (this.etatJeu != EtatJeu.EN_COURS) {
            throw new IllegalStateException("Le jeu est terminé");
        }
        // Le paramètre colonne est valide
        if (colonne < 0 || colonne >= NOMBRE_COLONNES) {
            throw new IllegalArgumentException("Numéro de colonne saisi invalide");
        }
        // On récupère le dernier emplacement libre de la colonne
        int ligne = this.getHauteur(colonne);
        if (ligne == -1) {
            throw new IllegalStateException("La colonne saisie ne dispose pas d'emplacement libre");
        }
        this.grilleJeu[ligne][colonne] = this.joueurCourant;
        testerJeu(this.joueurCourant);
        // On met à jour le joueur courant
        this.joueurCourant = this.getProchain();
    }

    /**
     * @param colonne de 0 à NOMBRE_COLONNES
     * @return le dernier emplacement libre (= hauteur) d'une colonne : nombre
     * entre 0 et NOMBRE_LIGNE, -1 si aucun emplacement n'est libre.
     */
    private int getHauteur(final int colonne) {
        for (int i = (NOMBRE_LIGNES - 1); i >= 0; i--) {
            if (this.grilleJeu[i][colonne] == '-') {
                return i;
            }
        }
        return -1;
    }

    /**
     * @param joueur : J ou R . Analyse la configuration du jeu afin de tester
     * l'état du jeu. 1 -> On teste si la configuration du joueur courant donne
     * lieu à un résultat. 2 -> On teste si la configuration donne lieu à un
     * match nul.
     */
    private void testerJeu(final char joueur) {
        if (tester4JetonsAxe(joueur)) {
            if (joueur == 'J') {
                this.etatJeu = EtatJeu.JAUNE_GAGNE;
            } else {
                this.etatJeu = EtatJeu.ROUGE_GAGNE;
            }
        }
        if (!this.emplacementRestant() && this.etatJeu == EtatJeu.EN_COURS) {
            this.etatJeu = EtatJeu.MATCH_NUL;
        }
    }

    /**
     * @param joueur : J ou R . Analyse la configuration du jeu afin de tester
     * l'état du jeu. On parcours la grille en testant les 4 axes : horizontal,
     * vertical, diagonale gauche & diagonale droite.
     * @return true si la configuration du joueur est gagnante sinon false.
     */
    private boolean tester4JetonsAxe(final char joueur) {
        int compteurLigne[] = new int[NOMBRE_LIGNES];
        int compteurColonne[] = new int[NOMBRE_COLONNES];
        int compteurDiagonaleGauche[] = new int[NOMBRE_LIGNES];
        int compteurDiagonaleDroite[] = new int[NOMBRE_LIGNES];

        for (int i = 0; i < NOMBRE_LIGNES; i++) {
            for (int j = 0; j < NOMBRE_COLONNES; j++) {
                int diagonaleGauche = calculerDiagonaleGauche(i, j);
                int diagonaleDroite = calculerDiagonaleDroite(i, j);

                if (this.grilleJeu[i][j] == joueur) {
                    compteurLigne[i]++;
                    compteurColonne[j]++;

                    if (compteurLigne[i] == 4 || compteurColonne[j] == 4) {
                        return true;
                    }

                    // Si on se trouve sur une diagonale gauche valide
                    if (diagonaleGauche >= 0) {
                        compteurDiagonaleGauche[diagonaleGauche]++;

                        if (compteurDiagonaleGauche[diagonaleGauche] == 4) {
                            return true;
                        }
                    }
                    // Si on se trouve sur une diagonale droite valide
                    if (diagonaleDroite >= 0) {
                        compteurDiagonaleDroite[diagonaleDroite]++;

                        if (compteurDiagonaleDroite[diagonaleDroite] == 4) {
                            return true;
                        }
                    }

                } else {
                    compteurLigne[i] = 0;
                    compteurColonne[j] = 0;

                    // Si on se trouve sur une diagonale gauche valide
                    if (diagonaleGauche >= 0) {
                        compteurDiagonaleGauche[diagonaleGauche] = 0;
                    }

                    // Si on se trouve sur une diagonale droite valide
                    if (diagonaleDroite >= 0) {
                        compteurDiagonaleDroite[diagonaleDroite] = 0;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param ligne.
     * @param colonne.
     * @return un identifiant de diagonale gauche si coordonnées valides, -1
     * sinon. Un identifiant de diagonale gauche est calculé de la manière
     * suivante : on récupère l'intervalle entre la ligne et la colonne. Si
     * ligne est inférieur à colonne on teste que l'intervalle inférieur-égal à
     * 3, si colonne est inférieur à la ligne on teste que l'intervalle est
     * inférieur à 2. Ex. [0,0] => diagonale Gauche valide 0; [0,1] => diagonale
     * gauche valide 1 ; [0,4] => diagonale gauche invalide (-1) car cette
     * diagonale ne contient pas 4 emplacements pour les jetons.
     */
    private int calculerDiagonaleGauche(final int ligne, final int colonne) {
        return (ligne <= colonne) ? ((colonne - ligne) <= 3 ? (colonne - ligne) : -1) : (((ligne - colonne) < 2) ? ((ligne - colonne) + 3) : -1);
    }

    /**
     * @param ligne.
     * @param colonne.
     * @return un identifiant de diagonale droite si coordonnées valides, -1
     * sinon. Un identifiant de diagonale droite est calculé de la manière
     * suivante : On additionne ligne et colonne puis on soustrait 3. Si le
     * resultat est supérieur à 0 et inférieur à 6 alors la diagonale est
     * valide. Ex. [0,0] => diagonale Droite invalide (-1); [0,3] => diagonale
     * droite valide 0 ; [1,0] => diagonale droite invalide (-1) car cette
     * diagonale ne contient pas 4 emplacements pour les jetons.
     */
    private int calculerDiagonaleDroite(final int ligne, final int colonne) {
        return ((ligne + colonne - 3) >= 0 && (ligne + colonne - 3) < 6) ? (ligne + colonne - 3) : -1;
    }

    /**
     * Teste s'il reste des emplacements libres dans la grille de jeu. On se
     * contente de parcourir le sommet de la grille (= la dernière ligne)
     * puisque les jetons sont insérés par le bas de la grille.
     */
    private boolean emplacementRestant() {
        for (int j = 0; j < NOMBRE_COLONNES; j++) {
            if (this.grilleJeu[0][j] == '-') {
                return true;
            }
        }
        return false;
    }

}
